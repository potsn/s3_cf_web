terraform {
  backend "s3" {
    bucket = "tf-state-study"
    key    = "cloudf-s3.terraform.tfstate"
    region = "eu-west-1"
  }
}