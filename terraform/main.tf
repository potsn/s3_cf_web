
// Ref. URL : https://medium.com/runatlantis/hosting-our-static-site-over-ssl-with-s3-acm-cloudfront-and-terraform-513b799aec0f

provider "aws" {
  region = "us-east-1"
}

// Create a variable for our domain name because we'll be using it a lot.
variable "www_domain_name" {
  default = "www.test-hyonsok.com" // 변경 필요, 본인의 도메인을 입력해야 함. (Route53에서 구매한 도메인)
}

// We'll also need the root domain (also known as zone apex or naked domain).
variable "root_domain_name" {
  default = "test-hyonsok.com"
}

variable "domain_zone_id" {
  default = "Z1JW0NMKG0UJV" // 변경 필요, 본인 도메인의 Route53 Zone Id를 입력해야 함.
}
